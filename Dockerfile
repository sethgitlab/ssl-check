FROM alpine:latest
RUN apk add --update coreutils openssl && rm -rf /var/cache/apk/*

ADD analyze /analyze
RUN chmod +x /analyze

# ENTRYPOINT []
# CMD ["/analyze"]
